namespace SortingAlgorithms
{
    internal class Program
    {
        static void Main(string[] args)
        {

            int[] input = new int[] { 5, 2, 1, 4, 28 };
            int[] output = InsertionSort(input);

            Console.ReadKey();
        }

        public static int[] InsertionSort(int[] unsorted)
        {
            int[] sorted = new int[unsorted.Length];
            int index = 0;
            string unsLog = "";
            string log = "";

            while (index < unsorted.Length)
            {
                unsLog = unsLog + " " + unsorted[index];

                index++;
            }

            index = 0;

            while (index < sorted.Length)
            {
                int i = 0;
                int max = Int32.MinValue;
                int maxIndex = 0;

                while (i < unsorted.Length)
                {
                    if (unsorted[i] >= max)
                    {
                        max = unsorted[i];
                        maxIndex = i;
                    }

                    i++;
                }

                unsorted[maxIndex] = Int32.MinValue;
                sorted[index] = max;

                log = log + " " + max;

                index++;
            }

            Console.WriteLine("Unsorted:" + unsLog);
            Console.WriteLine("Sorted:" + log);

            return sorted;
        }

    }
}